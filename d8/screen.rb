#!/usr/bin/env ruby

ROWS = 6
COLUMNS = 50

#ROWS = 3
#COLUMNS = 7

ON = '#'
OFF = '.'

$grid = (0...ROWS).map do
  [OFF] * COLUMNS
end

def print_grid
  $grid.each do |line|
    puts line.join('')
  end
end

def parse(line)
  tokens = line.split
  opcode = tokens[0]

  {}.tap do |command|
    command[:opcode] = opcode

    case opcode
    when 'rect'
      dimensions = tokens[1].split('x')
      command[:width] = dimensions[0].to_i
      command[:height] = dimensions[1].to_i

    when 'rotate'
      command[:dimension] = tokens[1]
      command[:coordinate] = tokens[2].split('=')[1].to_i
      command[:distance] = tokens[4].to_i
    end
  end
end

def run_command(command)
  case command[:opcode]
  when 'rect'
    do_rect(command)
  when 'rotate'
    do_rotate(command)
  end
end

def do_rect(command)
  (0...command[:height]).each do |r|
    (0...command[:width]).each do |c|
      set_on(r, c)
    end
  end
end

def do_rotate(command)
  if command[:dimension] == 'row'
    row = command[:coordinate]
    dist = command[:distance]
    dist.times do
      last_el = $grid[row][COLUMNS - 1]
      (1...COLUMNS).reverse_each do |col|
        $grid[row][col] = $grid[row][col - 1]
      end
      $grid[row][0] = last_el
    end
  else
    col = command[:coordinate]
    dist = command[:distance]
    dist.times do
      last_el = $grid[ROWS - 1][col]
      (1...ROWS).reverse_each do |row|
        $grid[row][col] = $grid[row - 1][col]
      end
      $grid[0][col] = last_el
    end
  end
end

def set_on(row, col)
  $grid[row][col] = ON
end

def set_off(row, col)
  $grid[row][col] = OFF
end

STDIN.read.lines.each do |line|
  cmd = parse(line)
  run_command(cmd)
end

print_grid

count = $grid.map do |row|
  row.join('')
end.join('').count('#')

puts count
