#! /usr/bin/env ruby

N = 0
E = 1
S = 2
W = 3

$direction = N

$position = [ 0, 0 ]

$instructions = STDIN.read.split(',').map(&:strip).map do |s|
  { turn: s[0], steps: s.slice(1, 9999999) }
end

$visited = {}

def make_turn(inst)
  $direction += inst[:turn] == 'R' ? 1 : -1
  $direction %= 4
end

def take_steps(steps)
  steps = steps.to_i
  case $direction
  when N
    $position[1] += steps
  when E
    $position[0] += steps
  when S
    $position[1] -= steps
  when W
    $position[0] -= steps
  end
end

def visited_key
  $position.join(',')
end

def visited?
  $visited[visited_key]
end

def mark_visited
  $visited[visited_key] = true
end

mark_visited
$instructions.each do |inst|
  make_turn(inst)
  #puts "Dir: #{$direction}"
  steps = inst[:steps].to_i
  steps.times do
    take_steps(1)
    #puts "Pos: #{$position}"
    if visited?
      puts "Visited twice: #{$position.inspect}"
    end
    mark_visited
    #puts $visited.inspect
  end
end

puts $position.inspect
