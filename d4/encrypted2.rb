#!/usr/bin/env ruby

require 'ostruct'

$sum = 0

def parse(line)
  line.match(/([a-z-]+)([0-9]+)\[([a-z]+)\]/)
  OpenStruct.new({
    name: $1,
    number: $2.to_i,
    checksum: $3
  })
end

def compute_checksum(name)
  letters = Hash.new(0)
  name.gsub(/[^a-z]/, '').split('').each do |letter|
    letters[letter] += 1
  end
  letters.keys.sort do |a, b|
    if letters[a] != letters[b]
      letters[b] <=> letters[a]
    else
      a <=> b
    end
  end.join.slice(0, 5)
end

def decrypt(name, number)
  start = 'a'.ord
  n = 'z'.ord + 1 - start
  name.split('').map do |char|
    if char == '-'
      ' '
    else
      (( char.ord - start + number ) % n + start).chr
    end
  end.join('')
end

STDIN.read.lines.each do |line|
  room = parse(line)
  check = compute_checksum(room.name)
  puts "#{decrypt(room.name, room.number)} -- #{room.number}"
end
