#!/usr/bin/ruby

$frequencies = nil

STDIN.read.lines.each do |line|
  $frequencies ||= [].tap do |arr|
    line.length.times do
      arr << Hash.new(0)
    end
  end
  $frequencies.each_with_index do |h, i|
    h[line[i]] += 1
  end
end

puts $frequencies.inspect

letters = $frequencies.map do |h|
  (h.keys.sort do |a, b|
    h[a] <=> h[b]
  end)[0]
end

puts letters.join('')
