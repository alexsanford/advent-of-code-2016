#!/usr/bin/env ruby

require 'digest'

i = 0
n = 0

input = 'wtnhxymk'

while true
  hash = Digest::MD5.hexdigest("#{input}#{i}")

  if hash.slice(0, 5) == '00000'
    puts hash[5] 
    n += 1
  end
  #puts i if i % 100000 == 0
  break if n >= 8
  i += 1
end
