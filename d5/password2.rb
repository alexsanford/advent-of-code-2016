#!/usr/bin/env ruby

require 'digest'

i = 0
n = 0

input = 'wtnhxymk'
$password = '________'

while true
  hash = Digest::MD5.hexdigest("#{input}#{i}")

  if hash.slice(0, 5) == '00000' && hash[5].match(/[0-9]/)
    position = hash[5].to_i
    char = hash[6]
    puts "index: #{i}"
    puts "hash: #{hash}"
    puts "position: #{position}"
    puts "char: #{char}"
    puts $password[position]
    if $password[position] == '_'
      $password[position] = char
      puts $password
    end
  end
  #puts i if i % 100000 == 0
  break unless $password.match(/_/)
  i += 1
end
