#!/usr/bin/env ruby

def supports_tls?(ip_address)
  address_parts = []
  hypernet_parts = []
  ip_address.split(/[\[\]]/).each_with_index do |part, index|
    if index % 2 == 0
      address_parts << part
    else
      hypernet_parts << part
    end
  end

  hypernet_parts.each do |part|
    return false if has_abba?(part)
  end

  address_parts.each do |part|
    return true if has_abba?(part)
  end

  return false
end

def has_abba?(part)
  start_index = 0
  end_index = part.size - 4

  (start_index..end_index).each do |i|
    subset = part.slice(i, 4)
    if subset[0] == subset[3] && subset[1] == subset[2] && subset[0] != subset[1]
      return true
    end
  end

  return false
end

$n = 0

STDIN.read.lines.map(&:strip).each do |ip_address|
  $n += 1 if supports_tls?(ip_address)
end

puts $n
