#!/usr/bin/env ruby

def supports_ssl?(ip_address)
  address_parts = []
  hypernet_parts = []
  ip_address.split(/[\[\]]/).each_with_index do |part, index|
    if index % 2 == 0
      address_parts << part
    else
      hypernet_parts << part
    end
  end

  hypernet_bab_list = []
  hypernet_parts.each do |part|
    hypernet_bab_list += get_bab_list(part)
  end

  address_aba_list = []
  address_parts.each do |part|
    address_aba_list += get_aba_list(part)
  end

  address_aba_list.each do |aba|
    hypernet_bab_list.each do |bab|
      return true if aba_bab_match?(aba, bab)
    end
  end

  return false
end

def get_aba_list(part)
  list = []
  start_index = 0
  end_index = part.size - 3

  (start_index..end_index).each do |i|
    subset = part.slice(i, 4)
    if subset[0] == subset[2] && subset[0] != subset[1]
      list << subset
    end
  end

  return list
end
alias :get_bab_list :get_aba_list

def aba_bab_match?(aba, bab)
  aba[0] == bab[1] && bab[0] == aba[1]
end

$n = 0

STDIN.read.lines.map(&:strip).each do |ip_address|
  $n += 1 if supports_ssl?(ip_address)
end

puts $n
