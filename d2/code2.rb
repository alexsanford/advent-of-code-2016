#!/usr/bin/env ruby

LEFT = 'L'
RIGHT = 'R'
UP = 'U'
DOWN = 'D'

A = 'A'
B = 'B'
C = 'C'
D = 'D'

_ = nil

$row = 3
$col = 1

$grid = [
  [ _, _, _, _, _, _, _ ],
  [ _, _, _, 1, _, _, _ ],
  [ _, _, 2, 3, 4, _, _ ],
  [ _, 5, 6, 7, 8, 9, _ ],
  [ _, _, A, B, C, _, _ ],
  [ _, _, _, D, _, _, _ ],
  [ _, _, _, _, _, _, _ ]
]

$code = []

def move(direction)
  case direction
  when LEFT
    new_col = $col - 1
    $col = new_col if $grid[$row][new_col]
  when RIGHT
    new_col = $col + 1
    $col = new_col if $grid[$row][new_col]
  when UP
    new_row = $row - 1
    $row = new_row if $grid[new_row][$col]
  when DOWN
    new_row = $row + 1
    $row = new_row if $grid[new_row][$col]
  end
end

def press
  $code << $grid[$row][$col]
end

STDIN.read.lines.each do |line|
  line.split('').each do |direction|
    move(direction)
  end
  press
end

puts $code.inspect
