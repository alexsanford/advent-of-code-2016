#!/usr/bin/env ruby

LEFT = 'L'
RIGHT = 'R'
UP = 'U'
DOWN = 'D'

MIN_POS = 0
MAX_POS = 2

$row = 1
$col = 1

$grid = [
  [ 1, 2, 3 ],
  [ 4, 5, 6 ],
  [ 7, 8, 9 ]
]

$code = []

def add(n, amount)
  result = n + amount
  if result < MIN_POS
    MIN_POS
  elsif result > MAX_POS
    MAX_POS
  else
    result
  end
end

def move(direction)
  case direction
  when LEFT
    $col = add($col, -1)
  when RIGHT
    $col = add($col, 1)
  when UP
    $row = add($row, -1)
  when DOWN
    $row = add($row, 1)
  end
end

def press
  $code << $grid[$row][$col]
end

STDIN.read.lines.each do |line|
  line.split('').each do |direction|
    move(direction)
  end
  press
end

puts $code.inspect
