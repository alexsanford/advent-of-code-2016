#!/usr/bin/env ruby

require 'set'
require 'ostruct'

def parse(line)
  if line.match(/value ([0-9]+) goes to bot ([0-9]+)/)
    OpenStruct.new({
      opcode: 'init',
      value: $1.to_i,
      bot: $2.to_i
    })
  elsif line.match(/bot ([0-9]+) gives low to (output|bot) ([0-9]+) and high to (output|bot) ([0-9]+)/)
    OpenStruct.new({
      opcode: 'compare',
      bot: $1.to_i,
      low_receiver: $2,
      low_num: $3.to_i,
      high_receiver: $4,
      high_num: $5.to_i
    })
  else
    raise "Unsupported line: #{line}"
  end
end

def find_available_bot
  $bots.each do |bot, values|
    return bot if values.size > 1
  end
end

$bots = Hash.new { |h, k| h[k] = Set.new }
$outputs = Hash.new { |h, k| h[k] = Set.new }
$receivers = {
  'bot' => $bots,
  'output' => $outputs
}

$commands = {}

STDIN.read.lines.each do |line|
  command = parse(line)
  #puts command
  case command.opcode
  when 'init'
    $bots[command.bot] << command.value
  when 'compare'
    if $commands[command.bot]
      raise "Bot #{command.bot} already has a command!"
    end
    $commands[command.bot] = command
  end
end

COMPARE = [ 17, 61 ]

def execute_command(bot)
  command = $commands[bot]
  return false unless command

  low, high = $bots.delete(bot).to_a.sort
  if [ low, high ] == COMPARE
    puts "Bot #{bot} is comparing #{low} and #{high}"
  end

  $receivers[command.low_receiver][command.low_num] << low
  $receivers[command.high_receiver][command.high_num] << high
  true
end

loop do
  bot = find_available_bot
  break unless bot
  break unless execute_command(bot)
end

puts $bots.inspect
puts $outputs.inspect
