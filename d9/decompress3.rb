#!/usr/bin/env ruby

def get_size(str, start, stop)
  puts "#{start} -- #{stop} -- #{str.size}"

  return 0 if start >= stop

  if str[start] == '('
    close = str.index(')', start)
    len, reps = str.slice((start+1..close-1)).split('x').map(&:to_i)

    substr_start = close + 1
    raise 'Oh noes!' if substr_start + len > stop

    substr_size = get_size(str, substr_start, substr_start + len)
    return substr_size * reps + get_size(str, substr_start + len, stop)
  else
    section_end = str.index('(', start)
    if !section_end || section_end > stop
      return stop - start
    else
      return section_end - start + get_size(str, section_end, stop)
    end
  end
end

STDIN.read.lines.each do |line|
  line.strip!
  size = get_size(line, 0, line.size)
  puts size
end
