#!/usr/bin/env ruby

# This one isn't working. Starting from scratch in decompress3.rb

def find_open(str, start = 0)
  str.index(/\([0-9]+x[0-9]+\)/, start)
end

def find_close(str, open)
  str.index(')', open)
end

def decompress_one(str)
  result = []

  open = find_open(str)
  return str unless open

  result << str.slice(0, open)

  close = str.index(')', open)
  marker = str.slice((open+1..close-1))

  len, reps = marker.split('x').map(&:to_i)

  reps.times do
    result << str.slice(close+1, len)
  end

  result << str.slice((close+len+1..-1))

  result.join.strip
end

def parse(str)
  str.strip!
  [].tap do |parsed|
    start = 0
    loop do
      open = find_open(str, start)
      if open
        len = open - start
        parsed << len if len > 0
        close = find_close(str, open)
        parsed << str.slice((open..close))
        start = close + 1
      else
        remaining = str.size - start
        parsed << remaining if remaining > 0
        break
      end
    end
  end
end

def parsed_len(parsed)
  sum = 0
  parsed.each do |el|
    if el.kind_of? Integer
      sum += el
    else
      sum += el.size
    end
  end
  sum
end

def expand(parsed)
  # For performance reasons, reverse the entire `parsed` array and pop off the back
  parsed = parsed.reverse
  [].tap do |new_parsed|
    while parsed.size > 0
      el = parsed.pop
      if el.kind_of? Integer
        new_parsed.push(el)
      else
        len, reps = el.gsub(/[\(\)]/, '').split('x').map(&:to_i)
        next unless len > 1
        sub = []
        pl = 0
        while (pl = parsed_len(sub)) < len
          sub.push(parsed.pop)
        end

        extra = pl - len
        sub.push(sub.pop - extra)

        reps.times do
          sub.each do |s|
            new_parsed.push(s)
          end
        end
        new_parsed.push(extra) if extra > 0
      end
    end
  end
end

def compact(parsed)
  # For performance reasons, reverse the entire `parsed` array and pop off the back
  parsed = parsed.reverse
  [].tap do |new_parsed|
    while parsed.size > 0
      el = parsed.pop
      if el.kind_of?(Integer) && new_parsed.last.kind_of?(Integer)
        new_parsed.push(new_parsed.pop + el)
      else
        new_parsed.push(el)
      end
    end
  end
end

STDIN.read.lines.each do |line|
  puts '==========================='
  parsed = parse(line)
  #puts parsed.inspect
  while parsed.size > 1
    puts 'expanding...'
    parsed = expand(parsed)
    puts 'compacting...'
    parsed = compact(parsed)
    #puts parsed.inspect
    puts parsed.size
  end
  puts parsed.inspect
  #size = 0
  #while str.size > 0
  #  str = decompress_one(str)
  #  open = find_open(str)
  #  if open
  #    size += open
  #    str = str.slice((open..-1))
  #  else
  #    size += str.length
  #    str = ''
  #  end
  #  #puts "#{size} -- #{str.size} -- #{str.count('(')} => #{str}"
  #  puts "#{size} -- #{str.size} -- #{str.count('(')}"
  #end
  ##puts "\"#{answer}\" -- #{answer.size}"
  #puts size
end
