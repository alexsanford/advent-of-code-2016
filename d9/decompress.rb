#!/usr/bin/env ruby

STDIN.read.lines.each do |line|
  str = line
  result = []
  while str.size > 0
    open = str.index(/\([0-9]+x[0-9]+\)/)

    result << str and break unless open

    result << str.slice(0, open)

    close = str.index(')', open)
    marker = str.slice((open+1..close-1))

    len, reps = marker.split('x').map(&:to_i)

    reps.times do
      result << str.slice(close+1, len)
    end

    str = str.slice((close+len+1..-1))
  end

  answer = result.join.strip
  puts "\"#{answer}\" -- #{answer.size}"
end
