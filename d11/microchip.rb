#!/usr/bin/env ruby

require 'set'

class State
  attr_accessor :elevator
  attr_accessor :generators, :microchips

  def initialize
    @elevator = 1
    @generators = {}
    @microchips = {}
  end

  def make_copy
    State.new.tap do |s|
      s.elevator = @elevator
      s.generators = @generators.clone
      s.microchips = @microchips.clone
    end
  end

  def add_generator(type, floor)
    @generators[type] = floor
  end

  def add_microchip(type, floor)
    @microchips[type] = floor
  end

  def bottom?
    @elevator == floor_num_range.min
  end

  def top?
    @elevator == floor_num_range.max
  end

  def move(direction, generators, microchips)
    @floors = nil

    n = generators.length + microchips.length
    raise 'Can only move up to two objects' if n > 2
    raise 'Must move at least one object' if n < 1

    # Ensure all objects are on the floor of the elevator
    generators.each do |gen|
      unless @generators[gen] == elevator
        raise "Generator #{gen} is not on floor #{elevator}"
      end
    end
    microchips.each do |chip|
      unless @microchips[chip] == elevator
        raise "Microchip #{chip} is not on floor #{elevator}"
      end
    end

    if direction == :up
      raise 'Cannot move up past the top' if top?
    else
      raise 'Cannot move down past the bottom' if bottom?
    end

    new_floor = direction == :up ? elevator + 1 : elevator - 1

    generators.each do |gen|
      @generators[gen] = new_floor
    end
    microchips.each do |chip|
      @microchips[chip] = new_floor
    end
    @elevator = new_floor
  end

  def valid?
    floors.each do |floor_num, floor|
      next if floor[:generators].empty?
      floor[:microchips].each do |type|
        return false unless floor[:generators].include?(type)
      end
    end
    return true
  end

  def final?
    top_floor = floor_num_range.max
    floors.each do |floor_num, floor|
      if floor_num != top_floor
        return false unless floor[:generators].empty? && floor[:microchips].empty?
      end
    end
    return true
  end

  def floor_num_range
    (1..4)
  end

  def floors
    @floors ||= {}.tap do |floors|
      floor_num_range.each do |floor|
        floors[floor] = { generators: [], microchips: [] }
      end

      generators.each do |gen, floor|
        floors[floor][:generators] << gen
      end
      microchips.each do |chip, floor|
        floors[floor][:microchips] << chip
      end
    end
  end

  def current_floor
    floors[@elevator]
  end

  def possible_states
    items = [nil] + \
      self.current_floor[:generators].map { |g| { type: :generator, item: g } } + \
      self.current_floor[:microchips].map { |m| { type: :microchip, item: m } }

    n = items.length
    (0...n).each do |i|
      (i+1...n).each do |j|
        item1 = items[i]
        item2 = items[j]

        generators = []
        microchips = []

        [item1, item2].each do |item|
          if item
            generators << item[:item] if item[:type] == :generator
            microchips << item[:item] if item[:type] == :microchip
          end
        end

        next_state = self.make_copy
        unless next_state.top?
          next_state.move(:up, generators, microchips)
          yield next_state if next_state.valid?
        end

        next_state = self.make_copy
        unless next_state.bottom?
          next_state.move(:down, generators, microchips)
          yield next_state if next_state.valid?
        end
      end
    end
  end

  def to_grid_string
    floors = self.floors
    str = "===+===+=========================================\n"
    floor_num_range.reverse_each do |floor_num|
      items = floors[floor_num][:generators].map { |s| "G-#{s}" }
      items += floors[floor_num][:microchips].map { |s| "M-#{s}" }
      str += "F#{floor_num} | #{@elevator == floor_num ? 'E' : ' '} | #{items.join(' ')}\n"
      str += "---+---+-----------------------------------------\n"
    end
    str
  end

  def eql?(other_object)
    other_object.kind_of?(State) \
      && other_object.elevator.eql?(@elevator) \
      && other_object.generators.eql?(@generators) \
      && other_object.microchips.eql?(@microchips)
  end

  def hash
    @elevator.hash * 13 + @generators.hash * 23 + @microchips.hash * 31
  end
end

initial_state = State.new

# Example 1
#initial_state.add_microchip('hydrogen', 1)
#initial_state.add_microchip('lithium', 1)
#initial_state.add_generator('hydrogen', 2)
#initial_state.add_generator('lithium', 3)

# Input
initial_state.add_generator('promethium', 1)
initial_state.add_microchip('promethium', 1)
initial_state.add_generator('cobalt', 2)
initial_state.add_generator('curium', 2)
initial_state.add_generator('ruthenium', 2)
initial_state.add_generator('plutonium', 2)
initial_state.add_microchip('cobalt', 3)
initial_state.add_microchip('curium', 3)
initial_state.add_microchip('ruthenium', 3)
initial_state.add_microchip('plutonium', 3)

initial_state.add_generator('elerium', 1)
initial_state.add_microchip('elerium', 1)
initial_state.add_generator('dilithium', 1)
initial_state.add_microchip('dilithium', 1)

$queue = []
$visited = Set.new

def expand_next
  state, depth = $queue.shift
  puts "On depth: #{depth}"
  state.possible_states do |next_state|
    $queue << [ next_state, depth + 1 ] unless $visited.include?(next_state)
    $visited << next_state
  end
end

# Run BFS
$queue << [ initial_state, 0 ]
until $queue.empty? || $queue.first[0].final?
  expand_next
end


# Print solution
if $queue.empty?
  puts 'No Solution!'
else
  puts "Found final state!"
  puts $queue.first[0].to_grid_string
  puts $queue.first[1]
end
