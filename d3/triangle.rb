#!/usr/bin/env ruby

$num_triangles = 0

def is_triangle?(triangle)
  return triangle[0] + triangle[1] > triangle[2]
end

STDIN.read.lines.each do |line|
  triangle = line.split.map(&:to_i).sort
  $num_triangles += 1 if is_triangle?(triangle)
end

puts $num_triangles
