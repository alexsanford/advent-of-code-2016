#!/usr/bin/env ruby

$num_triangles = 0

def is_triangle?(triangle)
  return triangle[0] + triangle[1] > triangle[2]
end

$t0 = []
$t1 = []
$t2 = []

STDIN.read.lines.each do |line|
  sides = line.split.map(&:to_i)
  $t0 << sides[0]
  $t1 << sides[1]
  $t2 << sides[2]

  if $t0.size == 3
    [ $t0, $t1, $t2 ].each do |t|
      $num_triangles += 1 if is_triangle?(t.sort)
    end
    $t0 = []
    $t1 = []
    $t2 = []
  end
end

puts $num_triangles
